package org.bitbucket.deividasp.quicksort;

import java.util.Random;

/**
 * Rikiavimo testavimo klasė.
 */
public class SorterTest {

    /**
     * Reikšmių skaičius.
     */
    public static final int VALUE_COUNT = 10000;

    /**
     * Gijų skaičius.
     */
    public static final int THREAD_COUNT = 5;

    /**
     * Pagrindinis programos metodas. Čia programa pradeda savo darbą.
     */
    public static void main(String[] arguments) {
        Integer[] values = new Integer[VALUE_COUNT];

        Random random = new Random();

        for (int i = 0; i < VALUE_COUNT; i++) {
            values[i] = random.nextInt();
        }

        ParallelSorter sorter = new ParallelSorter(THREAD_COUNT);

        long startTime = System.currentTimeMillis();
        sorter.quicksort(values);

        System.out.println("Sorted in: " + (System.currentTimeMillis() - startTime) + "ms.");

        System.out.println("Sorted values: ");
        System.out.print("[ ");

        for (int v : values) {
            System.out.print(v + " ");
        }

        System.out.println("]");
    }

}