package org.bitbucket.deividasp.quicksort;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Klasė, kuri naudojama nustatyti kiek gijų bus naudojama rikiuojant pasirinktu rikiavimo algoritmu.
 */
public class ParallelSorter {

    /**
     * Numatytasis gijų skaičius (jei nenurodoma konstruktoriuje).
     */
    public static final int DEFAULT_THREAD_COUNT = Runtime.getRuntime().availableProcessors();

    /**
     * Gijų skaičius.
     */
    private final int threadCount;

    /**
     * Vykdytojas.
     */
    private final ExecutorService executor;

    /**
     * Numatytasis konstruktorius (parenka numatytajį gijų skaičių).
     */
    public ParallelSorter() {
        this(DEFAULT_THREAD_COUNT);
    }

    /**
     * Parametrizuotas konstruktorius (nurodomas gijų skaičius).
     */
    public ParallelSorter(int threadCount) {
        this.threadCount = threadCount;

        executor = Executors.newFixedThreadPool(threadCount);
    }

    /**
     * Išrikiuoja nurodytas reikšmės quicksort algoritmu.
     */
    public <T extends Comparable<T>> void quicksort(T[] values) {
        AtomicInteger currentThreadCount = new AtomicInteger(1);

        executor.execute(new ParallelQuicksort<>(executor, values, 0, values.length - 1, threadCount, currentThreadCount));

        try {
            synchronized (currentThreadCount) {
                currentThreadCount.wait();
            }
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        executor.shutdown();
    }

}