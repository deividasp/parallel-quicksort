package org.bitbucket.deividasp.quicksort;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Lygiagreti Quicksort algoritmo implementacija.
 */
public class ParallelQuicksort<T extends Comparable<T>> implements Runnable {

    /**
     * Vykdytojas.
     */
    private final Executor executor;

    /**
     * Reikšmės, kurios turi būti išrikiuotos.
     */
    private final T[] values;

    /**
     * Indeksas, kuris nurodo nuo kurios vietos rikiuoti.
     */
    private final int startIndex;

    /**
     * Indeksas, kuris nurodo iki kurios vietos rikiuoti.
     */
    private final int endIndex;

    /**
     * Gijų skaičius.
     */
    private final int threadCount;

    /**
     * Dabartinis gijų skaičius.
     */
    private final AtomicInteger currentThreadCount;

    /**
     * Parametrizuotas konstruktorius. Čia nurodomi visi rikiavimo parametrai.
     */
    public ParallelQuicksort(Executor executor, T[] values, int startIndex, int endIndex, int threadCount, AtomicInteger currentThreadCount) {
        this.executor = executor;
        this.values = values;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.threadCount = threadCount;
        this.currentThreadCount = currentThreadCount;
    }

    @Override
    public void run() {
        quicksort(startIndex, endIndex);

        synchronized (currentThreadCount) {
            if (currentThreadCount.getAndDecrement() == 1) {
                currentThreadCount.notify();
            }
        }
    }

    /**
     * Pagrindinis rikiavimo metodas. Čia algoritmo darbas paskirstomas gijomis.
     */
    private void quicksort(int startIndex, int endIndex) {
        if (startIndex >= endIndex) {
            return;
        }

        int pivotIndex = partition(startIndex, endIndex);

        if (currentThreadCount.get() >= 2 * threadCount) {
            quicksort(startIndex, pivotIndex - 1);
            quicksort(pivotIndex + 1, endIndex);
        } else {
            currentThreadCount.getAndAdd(2);

            executor.execute(new ParallelQuicksort<>(executor, values, startIndex, pivotIndex - 1, threadCount, currentThreadCount));
            executor.execute(new ParallelQuicksort<>(executor, values, pivotIndex + 1, endIndex, threadCount, currentThreadCount));
        }
    }

    /**
     * Perskirsto masyvų elementus tarp nurodytų indeksų taip, kad elementai mažesni už elementą, kuris
     * yra paskutintame nurodytame indekse (pivot elementas), atsiduria prieš šį elementą, o didesni arba lygūs - už jo,
     * ir grąžina naują pivot elemento indeksą.
     */
    private int partition(int startIndex, int endIndex) {
        int pivotIndex = startIndex;

        T pivotValue = values[endIndex];

        for (int i = startIndex; i < endIndex; i++) {
            if (values[i].compareTo(pivotValue) < 0) {
                swap(i, pivotIndex);
                pivotIndex++;
            }
        }

        swap(pivotIndex, endIndex);

        return pivotIndex;
    }

    /**
     * Apkeičia vietomis nurodytų indeksų reikšmes.
     */
    private void swap(int index1, int index2) {
        T tempValue = values[index1];

        values[index1] = values[index2];
        values[index2] = tempValue;
    }

}